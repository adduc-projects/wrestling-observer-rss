<?php

$feed->channel->title = $params['feed'] . ": " . $feed->channel->title;

foreach ($feed->channel->item as $item) {
    $item->title .= " - " . $item->pubDate;
    $json = json_encode([$user, $pass]);
    // Using your key to encrypt information
    $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);

    $message = sodium_crypto_secretbox($json, $nonce, $key);
    $item->enclosure['url'] = sprintf(
        "%s://%s/feed/%s/enclosure/%s?n=%s&k=%s",
        (getenv('IS_HTTPS') ? 'https' : 'http'),
        $_SERVER['HTTP_HOST'],
        $params['feed'],
        md5($item->enclosure['url']),
        urlencode(base64_encode($nonce)),
        urlencode(base64_encode($message))
    );
}

header('Content-Type: application/rss+xml; charset=utf-8');
echo $feed->asXml();
