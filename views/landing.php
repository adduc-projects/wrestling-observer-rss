<?php

/** @todo move to app */
$shows = [
    "f4wdaily" => "All Shows",
    "wor" => "Wrestling Observer Radio",
    "wol" => "Wrestling Observer Live",
    "f4d" => "Figure Four Daily",
    "bandv" => "Bryan & Vinny Show",
    "afterdark" => "After Dark Radio",
    "wweekly" => "Wrestling Weekly",
    "anm" => "Adam & Mike Show",
    "karlstern" => "Karl Stern Show",
    "drkeith" => "Dr. Keith Presents with Alan 4L",
    "punchout" => "Josh Nason's Punch-Out",
    "classic" => "F4W Classics",
];

?>
<ul>
<?php foreach ($shows as $link => $show) { ?>
    <li><a href="/feed/<?=$link?>"><?=htmlentities($show)?></a></li>
<?php } ?>
</ul>


<?php foreach ($shows as $link => $show) { ?>
    <link
        rel="alternate"
        type="application/rss+xml"
        title="<?=htmlspecialchars($show)?>"
        href="/feed/<?=$link?>"
    >
<?php } ?>