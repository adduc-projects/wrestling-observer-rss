<?php

declare(strict_types=1);

namespace Adduc\WrestlingObserver;

use \Doctrine\Common\Cache;

class App
{
    protected $cache;
    protected $key;

    public function __construct()
    {
        $this->cache = new Cache\FilesystemCache(__DIR__ . '/../temp/cache');
        $this->key = base64_decode(getenv('APP_KEY'));
    }

    public function landing()
    {
        $this->render('landing');
    }

    public function feed(array $params)
    {
        list($user, $pass) = $this->checkAuth();
        $feed = $this->fetchFeed($params['feed']);
        $data = ['key' => $this->key] + compact('params', 'feed', 'user', 'pass');
        $this->render('feed', $data);
    }

    public function enclosure(array $params)
    {
        list($user, $pass) = $this->checkKey();
        $feed = $this->fetchFeed($params['feed']);
        $enclosure = $this->extractEnclosure($feed, $params['id']);
        $this->retrieveEnclosure($enclosure, $user, $pass);
    }

    protected function fetchFeed(string $name): \SimpleXMLElement
    {
        $feed = $this->cache->fetch($name);
        if (!$feed) {
            $feed = file_get_contents("https://www.f4wradio.com/feed/{$name}.rss");
            $this->cache->save($name, $feed, 30 * 60);
        }

        $options = LIBXML_COMPACT | LIBXML_NONET | LIBXML_NOERROR | LIBXML_NOWARNING;
        $feed = simplexml_load_string($feed, "SimpleXMLElement", $options);
        return $feed;
    }

    protected function checkAuth()
    {
        if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW'])) {
            $realm = "Unofficial Wrestling Observer RSS Feeds";
            header("WWW-Authenticate: Basic realm=\"{$realm}\"");
            header('HTTP/1.0 401 Unauthorized');
            exit;
        }

        return [
            $_SERVER['PHP_AUTH_USER'],
            $_SERVER['PHP_AUTH_PW']
        ];
    }

    protected function checkKey()
    {
        if (empty($_GET['k'])) {
            http_response_code(404);
            echo "No credentials provided.";
            exit;
        }

        $nonce = base64_decode($_GET['n']);
        $message = base64_decode($_GET['k']);

        $message = sodium_crypto_secretbox_open($message, $nonce, $this->key);

        if (!$message) {
            http_response_code(400);
            echo "Credentials message could not be decoded.";
            exit;
        }

        return json_decode($message, true);
    }

    protected function extractEnclosure(\SimpleXMLElement $feed, string $md5sum)
    {
        $enclosure = false;

        foreach ($feed->channel->item as $item) {
            if (md5(''.$item->enclosure['url']) == $md5sum) {
                $enclosure = $item->enclosure;
                break;
            }
        }

        if (!$enclosure) {
            http_response_code(404);
            echo "No episode found.";
            exit;
        }

        return $enclosure;
    }

    protected function retrieveEnclosure(\SimpleXMLElement $enclosure, string $user, string $pass)
    {
        $file = __DIR__ . '/../temp/cache/' . md5('' . $enclosure['url']);
        $client = new \GuzzleHttp\Client;

        // @todo consider timed cache for file (in case file is reuploaded)
        if (!file_exists($file)) {
            try {
                $res = $client->get(''.$enclosure['url'], [
                    'sink' => $file,
                    'auth' => [$user, $pass],
                ]);
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                http_response_code(403);
                echo "Invalid Credentials";
                exit;
            }

            $this->outputEnclosure($enclosure, $file);
        }

        // @todo consider caching credentials check
        try {
            $res = $client->head(''.$enclosure['url'], [
                'auth' => [$user, $pass],
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            http_response_code(403);
            echo "Invalid Credentials";
            exit;
        }

        $this->outputEnclosure($enclosure, $file);
    }

    protected function outputEnclosure($enclosure, $file)
    {
        header("Content-Type: " . $enclosure['type']);
        header("Content-Length: " . filesize($file));
        while (ob_get_level()) {
            ob_end_clean();
        }
        readfile($file);
        exit;
    }

    protected function render(string $view, array $params = [])
    {
        extract($params);
        require __DIR__ . sprintf("/../views/%s.php", $view);
    }
}
