<?php

declare(strict_types=1);

namespace Adduc\WrestlingObserver;

use FastRoute;

class Dispatcher
{
    public function dispatch()
    {
        $dispatcher = FastRoute\simpleDispatcher(function (FastRoute\RouteCollector $r) {
            $r->addRoute('GET', '/', 'landing');
            $r->addRoute('GET', '/feed/{feed}', 'feed');
            $r->addRoute('GET', '/feed/{feed}/enclosure/{id}', 'enclosure');
        });

        // Fetch method and URI from somewhere
        $httpMethod = $_SERVER['REQUEST_METHOD'];
        $uri = $_SERVER['REQUEST_URI'];

        // Strip query string (?foo=bar) and decode URI
        if (false !== $pos = strpos($uri, '?')) {
            $uri = substr($uri, 0, $pos);
        }
        $uri = rawurldecode($uri);
        $routeInfo = $dispatcher->dispatch($httpMethod, $uri);

        switch ($routeInfo[0]) {
            case FastRoute\Dispatcher::NOT_FOUND:
            case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
                http_response_code(404);
                echo "Not Found";
                break;
            case FastRoute\Dispatcher::FOUND:
                $app = new App();
                $handler = $routeInfo[1];
                $vars = $routeInfo[2];
                $app->{$handler}($vars);
                break;
        }
    }
}
