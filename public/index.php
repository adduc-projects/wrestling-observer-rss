<?php

require __DIR__ . '/../dependencies/vendor/autoload.php';

(new josegonzalez\Dotenv\Loader(__DIR__ . '/../.env'))
    ->parse()
    ->putenv();

$dispatcher = new Adduc\WrestlingObserver\Dispatcher();
$dispatcher->dispatch();
